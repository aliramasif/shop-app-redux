import { combineReducers } from "redux";
import { initializeProductsReducer } from "./initializeProductsReducer";
import { basketReducer } from "./basketReducer";
import { favouritesReducer } from "./favouritesReducer";
import { selectedProductReducer } from "./selectedProductReducer";
import { modalReducer } from "./modalReducer";

const rootReducer = combineReducers({
  fetchProducts: initializeProductsReducer,
  basketProducts: basketReducer,
  favouritedProducts: favouritesReducer,
  modal: modalReducer,
  selectedProduct: selectedProductReducer,
});

export default rootReducer;
