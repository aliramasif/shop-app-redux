import { useDispatch } from "react-redux";
import { setSelectedProduct } from "../../store/actions/selectProduct";
import { openModal } from "../../store/actions/modalChanges";
import ModalData from "../../ModalData";
import "./Button.scss";

export default function Button({ backgroundColor, text, product }) {
  const dispatch = useDispatch();
  const findModalDataById = (modalId) =>
    ModalData.find(({ id }) => id === modalId);

  const handleOpenModalButton = (modalId) => {
    const ModalDataItem = findModalDataById(modalId);
    dispatch(setSelectedProduct(product));
    dispatch(openModal(ModalDataItem, product));
  };

  return (
    <button
      className="button-component"
      style={{ backgroundColor: backgroundColor }}
      onClick={() => handleOpenModalButton("modalOne")}
    >
      {text}
    </button>
  );
}
